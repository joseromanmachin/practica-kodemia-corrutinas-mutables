package com.kotlin.practicadecorrutinasfull.common.service

import com.kotlin.practicadecorrutinasfull.common.api.ApiClient
import com.kotlin.practicadecorrutinasfull.common.api.RetrofitInstance
import com.kotlin.practicadecorrutinasfull.common.model.PokemonResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class ServiceNetwork {
    private val retrofit = RetrofitInstance.getRetrofit().create(ApiClient::class.java)

    suspend fun obtenerPokemones(dato : Int) : Response<PokemonResponse>{
        return withContext(Dispatchers.IO){
            val respuesta = retrofit.obtenerPokemos(dato)
            respuesta
        }
    }
}
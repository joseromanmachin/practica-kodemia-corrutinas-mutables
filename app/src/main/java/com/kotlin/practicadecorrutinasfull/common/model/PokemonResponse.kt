package com.kotlin.practicadecorrutinasfull.common.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PokemonResponse(
    @Expose
    @SerializedName("count")
    var count: Int,
    @Expose
    @SerializedName("next")
    val next: String,
    @Expose
    @SerializedName("results")
    val results: ArrayList<ResultResponse>
)



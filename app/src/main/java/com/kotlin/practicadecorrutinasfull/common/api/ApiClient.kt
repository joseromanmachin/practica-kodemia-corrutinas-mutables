package com.kotlin.practicadecorrutinasfull.common.api

import com.kotlin.practicadecorrutinasfull.common.model.PokemonResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiClient {

    @GET("pokemon")
    suspend fun obtenerPokemos(@Query("limit") limit:Int ) : Response<PokemonResponse>


}
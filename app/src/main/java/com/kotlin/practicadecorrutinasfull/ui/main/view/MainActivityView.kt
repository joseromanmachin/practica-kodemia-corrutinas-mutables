package com.kotlin.practicadecorrutinasfull.ui.main.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kotlin.practicadecorrutinasfull.R
import com.kotlin.practicadecorrutinasfull.common.model.PokemonResponse
import com.kotlin.practicadecorrutinasfull.ui.main.view.adapters.PokemonAdapter
import com.kotlin.practicadecorrutinasfull.ui.main.viewmodel.MainViewModel

class MainActivityView : AppCompatActivity() {
    val viewModel: MainViewModel by viewModels()

    lateinit var button: Button
    lateinit var txt: EditText
    lateinit var recyclerView:RecyclerView
    lateinit var progresBar : ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_view)

        inicializarComponentes()
        observervadores()

        button.setOnClickListener {
            val numero = txt.text.toString().toInt()
            viewModel.obtenerPokemos(numero)
        }
    }

    private fun observervadores() {
        viewModel.pokemones.observe(this) { pokemones -> llenarRecylerview(pokemones) }

        viewModel.error.observe(this) { error ->
            Toast.makeText(this, error, Toast.LENGTH_LONG).show()
            Log.d("pokemon", error)
        }
        viewModel.cargando.observe(this){cargando ->
            cargando(cargando)
        }
    }

    private fun cargando(cargando: Boolean) {
        if (cargando){
            progresBar.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
        }else {
            progresBar.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
        }
    }

    private fun llenarRecylerview(pokemones: PokemonResponse) {
        val adapter = PokemonAdapter(pokemones.results)
        recyclerView.hasFixedSize()
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

      //  pokemones.results.forEach { Log.d("pokemon", it.name) }


    }

    private fun inicializarComponentes() {
        button = findViewById(R.id.button)
        txt = findViewById(R.id.text)
        progresBar = findViewById(R.id.progressBar)
        recyclerView = findViewById(R.id.recycler)

 //       val suma = operacion(1, 2, funcionSuma)

        //        val resta = operacion(10, 8, funcionResta)

  //      val multiplicacion = operacion(5,9){ x, y -> x*y}
    }

 /*   fun suma(numero1: Int, numero2: Int): Int {
        return numero1 + numero2
    }

    val funcionSuma = fun(x: Int, y: Int): Int = x + y
    val funcionResta = fun(x: Int, y: Int): Int = x - y

    private fun operacion(numero1: Int, numero2: Int, funcion: (Int, Int) -> Int): Int {

        return funcion(numero1, numero2)
    }*/
}
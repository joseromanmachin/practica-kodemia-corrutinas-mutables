package com.kotlin.practicadecorrutinasfull.ui.main.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kotlin.practicadecorrutinasfull.R
import com.kotlin.practicadecorrutinasfull.common.model.ResultResponse

class PokemonAdapter (val data : ArrayList<ResultResponse>): RecyclerView.Adapter<PokemonAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.tarjeta_pokemon, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textViewPokemon.text = data[position].name
    }

    override fun getItemCount(): Int = data.size


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewPokemon: TextView

        init {
            textViewPokemon = view.findViewById(R.id.textView)
        }
    }
}
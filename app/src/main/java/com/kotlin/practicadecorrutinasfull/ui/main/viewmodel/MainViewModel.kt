package com.kotlin.practicadecorrutinasfull.ui.main.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kotlin.practicadecorrutinasfull.common.model.PokemonResponse
import com.kotlin.practicadecorrutinasfull.common.service.ServiceNetwork
import kotlinx.coroutines.launch
import java.io.IOException

class MainViewModel : ViewModel() {

    //llamamos al servicio
    private val service = ServiceNetwork()


    val pokemones = MutableLiveData<PokemonResponse>()
    val error = MutableLiveData<String>()
    val cargando = MutableLiveData<Boolean>()



    fun obtenerPokemos(limit : Int){
        viewModelScope.launch {
            cargando.postValue(true)
            try {
                val respuestaPokemones = service.obtenerPokemones(limit)

                if (respuestaPokemones.isSuccessful){
                    pokemones.postValue(respuestaPokemones.body())
                }else {
                    error.postValue(respuestaPokemones.errorBody().toString())
                }
                cargando.postValue(false)
            }catch (err : IOException){
                error.postValue(err.localizedMessage)
                cargando.postValue(false)
            }
        }
    }
}